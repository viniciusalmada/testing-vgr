#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GraRen/canvas.hpp>
#include <GraRen/line.hpp>
#include <GraRen/rectangle.hpp>
#include <iostream>
#include <thread>
#include <vector>

constexpr int WIDTH = 640;
constexpr int HEIGHT = 480;

GraRen::Canvas* canvas = nullptr;

GraRen::Rectangle* racket1 = nullptr;
GraRen::Rectangle* ball = nullptr;

GraRen::Number factor = 1;
constexpr GraRen::Number BALL_SIDE = 20;

bool ball_up = true;
bool ball_left = true;

long glfwGetMS() { return static_cast<long>(glfwGetTime() * 1000); }

void moveBall()
{
  if (ball == nullptr)
    return;

  GraRen::Point current_center = ball->GetCenterPoint();

  if (ball_up)
  {
    // Verify top
    if (current_center.GetY() + BALL_SIDE / 2 >= HEIGHT) //  top touched
    {
      // printf("touched: %ld\n", glfwGetMS());
      ball_up = false;
    }
  }
  else
  {
    // Verify bottom
    if (current_center.GetY() - BALL_SIDE / 2 <= 0) //  bottom touched
    {
      // printf("touched: %ld\n", glfwGetMS());
      ball_up = true;
    }
  }

  if (ball_left)
  {
    // Verify left
    if (current_center.GetX() + BALL_SIDE / 2 >= WIDTH) //  left touched
    {
      // printf("touched: %ld\n", glfwGetMS());
      ball_left = false;
    }
  }
  else
  {
    // Verify right
    if (current_center.GetX() - BALL_SIDE / 2 <= 0) //  right touched
    {
      // printf("touched: %ld\n", glfwGetMS());
      ball_left = true;
    }
  }

  GraRen::Number vel = 1;
  GraRen::Number horizontal_number =
    current_center.GetX() + (ball_left ? +vel : -vel);
  GraRen::Number vertical_number = current_center.GetY() + (ball_up ? +vel : -vel);

  ball->SetCenterPoint({ horizontal_number, vertical_number });
}

/*! @brief The function pointer type for keyboard key callbacks.
 *
 *  This is the function pointer type for keyboard key callbacks.  A keyboard
 *  key callback function has the following signature:
 *  @code
 *  void function_name(GLFWwindow* window, int key, int scancode, int action,
 * int mods)
 *  @endcode
 *
 *  @param[in] window The window that received the event.
 *  @param[in] key The [keyboard key](@ref keys) that was pressed or released.
 *  @param[in] scancode The system-specific scancode of the key.
 *  @param[in] action `GLFW_PRESS`, `GLFW_RELEASE` or `GLFW_REPEAT`.  Future
 *  releases may add more actions.
 *  @param[in] mods Bit field describing which [modifier keys](@ref mods) were
 *  held down.
 *
 *  @sa @ref input_key
 *  @sa @ref glfwSetKeyCallback
 *
 *  @since Added in version 1.0.
 *  @glfw3 Added window handle, scancode and modifier mask parameters.
 *
 *  @ingroup input
 */
void onKeyCallback(GLFWwindow* window,
                   int key,
                   int /* scancode */,
                   int action,
                   int /* mods */)
{
  if (key == GLFW_KEY_ESCAPE)
    glfwSetWindowShouldClose(window, GLFW_TRUE);

  if ((key == GLFW_KEY_UP || key == GLFW_KEY_DOWN) &&
      (action == GLFW_PRESS || action == GLFW_REPEAT))
  {
    if (racket1 == nullptr)
      return;

    if (action == GLFW_PRESS)
      factor = 1;

    if (action == GLFW_REPEAT)
      factor += 1;

    if (factor > 10)
      factor = 10;

    auto pos = racket1->RetrievePositions();
    GraRen::Point bot_left = pos[0];
    GraRen::Point top_right = pos[2];

    int parcel;
    if (key == GLFW_KEY_UP)
      parcel = factor;
    else
      parcel = -factor;

    bot_left = GraRen::Point{ bot_left.GetX(), bot_left.GetY() + parcel };
    top_right = GraRen::Point{ top_right.GetX(), top_right.GetY() + parcel };

    racket1->SetBottomLeft(bot_left);
    racket1->SetTopRight(top_right);
    if (canvas)
      canvas->UpdateData();
  }
}

void updateBall(GLFWwindow* window)
{
  constexpr long ball_vel = 100;                      // px/s
  constexpr long ball_interval_ms = 1'000 / ball_vel; // s
  long next_move = 0;
  while (true)
  {
    long time = glfwGetMS();

    if (time >= next_move)
    {
      moveBall();
      next_move = time + ball_interval_ms;
    }

    if (glfwWindowShouldClose(window))
      break;
  }
}

void updateWindow(GLFWwindow* window)
{
  constexpr long FPS = 75;
  constexpr long interval_ms = 1000 / FPS;

  glfwSetTime(0.0);
  long next_up = 0;
  while (true)
  {
    long time = glfwGetMS();

    if (time >= next_up)
    {
      glClear(GL_COLOR_BUFFER_BIT);
      glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

      canvas->UpdateData();
      canvas->Render();

      glfwSwapBuffers(window);

      next_up = time + interval_ms;
    }

    glfwPollEvents();

    if (glfwWindowShouldClose(window))
      break;
  }
}

int main()
{
  if (glfwInit() == GLFW_FALSE)
  {
    std::cerr << "GLFW error" << std::endl;
    return EXIT_FAILURE;
  }

  auto* window = glfwCreateWindow(WIDTH, HEIGHT, "Test GraRen", nullptr, nullptr);
  glfwMakeContextCurrent(window);
  glfwSetWindowAttrib(window, GLFW_RESIZABLE, GLFW_FALSE);
  glfwSetKeyCallback(window, onKeyCallback);

  if (glewInit() != GLEW_OK)
  {
    std::cerr << "GLEW error" << std::endl;
    return EXIT_FAILURE;
  }

  canvas = new GraRen::Canvas{ WIDTH, HEIGHT };
  racket1 = new GraRen::Rectangle{ { 0, 0 }, 20, 80, GraRen::Color::RED };
  ball = new GraRen::Rectangle{ { WIDTH / 2 - 10, HEIGHT / 2 - 10 },
                             BALL_SIDE,
                             BALL_SIDE,
                             GraRen::Color::BLUE };

  canvas->AddElement(racket1);
  canvas->AddElement(ball);

  std::thread ball_thread{ updateBall, window };

  updateWindow(window);

  ball_thread.join();

  glfwTerminate();

  return 0;
}